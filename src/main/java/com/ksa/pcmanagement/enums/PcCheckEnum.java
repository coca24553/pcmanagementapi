package com.ksa.pcmanagement.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PcCheckEnum {
    NORMAL("정상"),
    FAULTY("불량");

    private final String name;
}

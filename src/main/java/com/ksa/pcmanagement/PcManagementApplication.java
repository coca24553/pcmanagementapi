package com.ksa.pcmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PcManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(PcManagementApplication.class, args);
    }

}

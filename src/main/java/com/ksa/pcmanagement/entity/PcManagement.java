package com.ksa.pcmanagement.entity;

import com.ksa.pcmanagement.enums.PcCheckEnum;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.security.PrivateKey;
import java.time.LocalDate;

@Entity
@Getter
@Setter
public class PcManagement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Short seatName;

    @Column(nullable = false)
    private LocalDate dateCreate;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private PcCheckEnum pcCheckEnum;

    @Column(columnDefinition = "TEXT")
    private String checkMemo;

    @Column(nullable = false)
    private Boolean isUpgrade;
}

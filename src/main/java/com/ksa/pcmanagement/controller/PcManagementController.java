package com.ksa.pcmanagement.controller;

import com.ksa.pcmanagement.entity.PcManagement;
import com.ksa.pcmanagement.medel.PcManagementItem;
import com.ksa.pcmanagement.medel.PcManagementRequest;
import com.ksa.pcmanagement.service.PcManagementService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/management")
public class PcManagementController {
    private final PcManagementService pcManagementService;

    @PostMapping("/pc")
    public String setManagement(@RequestBody PcManagementRequest request) {
        pcManagementService.setPcManagement(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<PcManagementItem> getPcManagements() {
        return pcManagementService.getPcManagements();
    }
}

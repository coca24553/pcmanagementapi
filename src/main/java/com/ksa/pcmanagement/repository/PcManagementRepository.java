package com.ksa.pcmanagement.repository;

import com.ksa.pcmanagement.entity.PcManagement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PcManagementRepository extends JpaRepository<PcManagement, Long> {
}

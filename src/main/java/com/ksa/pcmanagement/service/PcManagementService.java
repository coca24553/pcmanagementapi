package com.ksa.pcmanagement.service;

import com.ksa.pcmanagement.entity.PcManagement;
import com.ksa.pcmanagement.medel.PcManagementItem;
import com.ksa.pcmanagement.medel.PcManagementRequest;
import com.ksa.pcmanagement.repository.PcManagementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PcManagementService {
    private final PcManagementRepository pcManagementRepository;

    public void setPcManagement(PcManagementRequest request) {
        PcManagement addDate = new PcManagement();
        addDate.setSeatName(request.getSeatName());
        addDate.setDateCreate(LocalDate.now());
        addDate.setPcCheckEnum(request.getPcCheckEnum());
        addDate.setCheckMemo(request.getCheckMemo());
        addDate.setIsUpgrade(request.getIsUpgrade());

        pcManagementRepository.save(addDate);
    }

    public List<PcManagementItem> getPcManagements() {
        List<PcManagement> originList = pcManagementRepository.findAll();

        List<PcManagementItem> result = new LinkedList<>();

        for (PcManagement pcManagement : originList) {
            PcManagementItem addItem = new PcManagementItem();
            addItem.setId(pcManagement.getId());
            addItem.setSeatName(pcManagement.getSeatName());
            addItem.setDateCreate(pcManagement.getDateCreate());
            addItem.setPcCheckEnum(pcManagement.getPcCheckEnum().getName());
            addItem.setIsUpgrade(pcManagement.getIsUpgrade());

            result.add(addItem);
        }

        return result;
    }
}

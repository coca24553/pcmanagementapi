package com.ksa.pcmanagement.medel;

import com.ksa.pcmanagement.enums.PcCheckEnum;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PcManagementRequest {
    private Short seatName;
    @Enumerated(value = EnumType.STRING)
    private PcCheckEnum pcCheckEnum;
    private String checkMemo;
    private Boolean isUpgrade;
}

package com.ksa.pcmanagement.medel;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PcManagementItem {
    private Long id;
    private Short seatName;
    private LocalDate dateCreate;
    private String pcCheckEnum;
    private Boolean isUpgrade;
}
